# What?
Just a package, that enabled some experimental features

## V 20201004
* Drop enforcing aco, because it's the default in mesa 20.2

## V 20200911
* Drop sished for better performance

## V 20200411
enables
* Radeon SI / CIK support for AMDGPU (disables RADEON at the same time)
* Deep Color support
* ACO shader compiler and sished sheduler for vulkan
